from django.views.generic import TemplateView
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect

from .forms import LoginForm

class LoginView(TemplateView):
    template_name = "login.html"

    def post(self, request):
        username = password = ''
        if request.POST:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    if user.is_staff:
                        return redirect('/dashboard/')
                    else:
                        return redirect('/leads/')
            # form = LoginForm(request.POST)
            # if form.is_valid():
            #     username = request.POST['username']
            #     password = request.POST['password']
            #     user = authenticate(username=username, password=password)
            #     if user is not None:
            #         if user.is_active:
            #             login(request, user)
            #             if user.is_staff:
            #                 return redirect('/dashboard/')
            #             else:
            #                 return redirect('/leads/')
            # else:
            #     request.session['form_data'] = request.POST

        return redirect('/login/')

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context['title'] = "Log In"

        return context


class LogoutView(TemplateView):

    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('/login/')
