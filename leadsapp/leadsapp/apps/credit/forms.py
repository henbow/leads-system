from django.forms import ModelForm

from leadsapp.leadsapp.apps.users.models import User
from leadsapp.leadsapp.apps.credit.models import UserCreditUsageHistory as CreditUsage


class UserCreditForm(ModelForm):
	class Meta:
		model = User
		fields = ['id', 'credit_balance', ]

class UserCreditHistoryForm(ModelForm):
	class Meta:
		model = CreditUsage
		fields = ['']
