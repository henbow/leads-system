from __future__ import unicode_literals

from django.db import models

class UserCreditUsageHistory(models.Model):
	user = models.ForeignKey('auth.User', on_delete=models.CASCADE, null=True)
	credit_amount = models.IntegerField(default=0)
	description = models.TextField(default='')
	transaction_date = models.DateTimeField(null=True)

	class Meta:
		db_table = 'user_credit_usages'