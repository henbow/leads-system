from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic

from leadsapp.leadsapp.apps.users.models import User

class CreditView(generic.ListView):
    template_name = 'credit/user_credit_list.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        return User.objects.order_by('-pub_date')[:5]