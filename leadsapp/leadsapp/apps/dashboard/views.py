from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


class DashboardView(TemplateView):
    template_name = "dashboard/admin_dashboard.html"

    @method_decorator(login_required(login_url="/login/"))
    def dispatch(self, *args, **kwargs):
        return super(DashboardView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['title'] = "Dashboard"
        context['sub_title'] = "dashboard & statitics"

        return context

class UserDashboardView(TemplateView):
    template_name = "dashboard/user_dashboard.html"

    @method_decorator(login_required(login_url="/login/"))
    def dispatch(self, *args, **kwargs):
        return super(UserDashboardView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UserDashboardView, self).get_context_data(**kwargs)
        context['title'] = "Dashboard"
        context['sub_title'] = "dashboard & statitics"

        return context