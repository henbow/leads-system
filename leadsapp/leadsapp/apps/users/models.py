from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User as CoreUser

class User(CoreUser):
	is_trial = models.BooleanField(default=1)
	credit_balance = models.IntegerField(default=2)
	result_limit = models.IntegerField(default=30)

	class Meta:
		db_table = 'user_meta'

	def __str__(self):
		return '{0} {1}'.format(self.first_name, self.last_name)

	def __unicode__(self):
		self.__str__()

