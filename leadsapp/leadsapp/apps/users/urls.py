from django.conf.urls import url

from leadsapp.apps.users import views

urlpatterns = [
    url(r'^lists/$', views.UserListView.as_view(), name='user_lists'),
    url(r'^create/$', views.UserCreateView.as_view(), name='user_create'),
    url(r'^edit/(?P<pk>\d+)/$', views.UserUpdateView.as_view(), name='user_edit'),
    url(r'^delete/(?P<pk>\d+)/$', views.UserDeleteView.as_view(), name='user_delete'),
]