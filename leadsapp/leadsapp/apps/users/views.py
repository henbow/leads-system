from django.core.urlresolvers import reverse_lazy
from django.views import generic

from .models import User
from .forms import UserForm

from datetime import datetime


class UserListView(generic.ListView):
    model = User
    template_name = 'user/user_list.html'
    paginate_by = 25
    context_object_name = 'users'

    def get(self, request, *args, **kwargs):
        self.request = request
        return super(UserListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(UserListView, self).get_context_data(**kwargs)
        data['title'] = "User Management"
        data['sub_title'] = "user lists"
        data['initial_number'] = 1

        return data

    def get_queryset(self):
        keywords = self.request.GET.get('keywords')
        join_date = self.request.GET.get('j_date')

        user_lists = User.objects.order_by('-date_joined')
        if keywords:
            user_lists = user_lists.filter(username__contains=keywords)

        if join_date:
            join_date_1 = datetime.strptime(join_date,'%m/%d/%Y').strftime('%Y-%m-%d 00:00:00.000000')
            join_date_2 = datetime.strptime(join_date,'%m/%d/%Y').strftime('%Y-%m-%d 23:59:59.999999')
            user_lists = user_lists.filter(date_joined__gte=join_date_1)
            user_lists = user_lists.filter(date_joined__lte=join_date_2)

        return user_lists


class UserCreateView(generic.CreateView):
    template_name = 'user/user_form.html'
    form_class = UserForm
    success_url = reverse_lazy('user_lists')
    model = User

    def get_context_data(self, **kwargs):
        data = super(UserCreateView, self).get_context_data(**kwargs)
        data['title'] = "User Management"
        data['sub_title'] = "add new user"
        data['mode'] = 'create'
        data['btn_submit_text'] = "Save"
        return data


class UserUpdateView(generic.UpdateView):
    form_class = UserForm
    template_name = 'user/user_form.html'
    model = User
    context_object_name = 'form'
    success_url = reverse_lazy('user_lists')

    def get_context_data(self, **args):
        data = super(UserUpdateView, self).get_context_data(**args)
        data['title'] = "User Management"
        data['sub_title'] = "edit user"
        data['mode'] = 'update'
        data['btn_submit_text'] = "Save"
        return data


class UserDeleteView(generic.DeleteView):
    model = User
    success_url = reverse_lazy('user_lists')
