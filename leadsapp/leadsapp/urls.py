"""leadsapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from leadsapp.apps.auth_user.views import LoginView as login
from leadsapp.apps.auth_user.views import LogoutView as logout
from leadsapp.apps.dashboard.views import DashboardView as dashboard

urlpatterns = [
    url(r'^$', dashboard.as_view()),
    url(r'^dashboard/$', dashboard.as_view()),
    url(r'^user/', include('leadsapp.apps.users.urls')),
    url(r'^leads/', dashboard.as_view()),
    url(r'^credits/', dashboard.as_view()),
    url(r'^leads_engine/', dashboard.as_view()),
    url(r'^login/$', login.as_view()),
    url(r'^logout/$', logout.as_view()),
    url(r'^admin/', include(admin.site.urls)),
]
